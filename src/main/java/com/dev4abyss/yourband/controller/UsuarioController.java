package com.dev4abyss.yourband.controller;

import com.dev4abyss.yourband.dto.UsuarioAuthDTO;
import com.dev4abyss.yourband.dto.UsuarioDTO;
import com.dev4abyss.yourband.entity.Usuario;
import com.dev4abyss.yourband.exception.AuthException;
import com.dev4abyss.yourband.mapper.UsuarioMapper;
import com.dev4abyss.yourband.services.UsuarioService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/usuarios")
@Tag(name = "Usuários", description = "Recursos Sobre Usuários")
public class UsuarioController {


    private final UsuarioService service;
    private final UsuarioMapper mapper;

    @GetMapping
    public ResponseEntity<List<UsuarioDTO>> listarTudo() {
        return ResponseEntity.ok(mapper.toDto(service.listarTudo()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioDTO> buscarPorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(mapper.toDto(service.buscarPorId(id)));
    }

    @PostMapping
    public ResponseEntity<UsuarioDTO> salvar(@RequestBody UsuarioDTO dto) {
        return new ResponseEntity(mapper.toDto(service.salvar(dto)), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<UsuarioDTO> editar(UsuarioDTO dto) {
        return new ResponseEntity(mapper.toDto(service.editarUsuario(dto)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable("id") Long id) {
        service.deletarUsuario(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/autenticar")
    public ResponseEntity autenticar(@RequestBody UsuarioAuthDTO dto) {
        try {
            Usuario usuarioAutenticado = service.autenticar(dto.getEmailOuLogin(), dto.getSenha());
            return ResponseEntity.ok(mapper.toDto(usuarioAutenticado));
        } catch (AuthException e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }
    }


}

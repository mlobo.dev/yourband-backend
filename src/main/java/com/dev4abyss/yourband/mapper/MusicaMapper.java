package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.MusicaDTO;
import com.dev4abyss.yourband.entity.Musica;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MusicaMapper extends BaseMapper<Musica, MusicaDTO> {

}

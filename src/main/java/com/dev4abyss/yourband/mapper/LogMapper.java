package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.LogDTO;
import com.dev4abyss.yourband.entity.Log;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LogMapper extends BaseMapper<Log, LogDTO> {

}

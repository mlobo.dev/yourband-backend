package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.RepertorioDTO;
import com.dev4abyss.yourband.entity.Repertorio;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RepertorioMapper extends BaseMapper<Repertorio, RepertorioDTO> {

}

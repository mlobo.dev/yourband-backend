package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.UsuarioDTO;
import com.dev4abyss.yourband.entity.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UsuarioMapper extends BaseMapper<Usuario, UsuarioDTO> {

}

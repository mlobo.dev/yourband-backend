package com.dev4abyss.yourband.repositories;

import com.dev4abyss.yourband.entity.Musica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MusicaRepository extends JpaRepository<Musica, Long> {

    List<Musica> findAllByNomeContainsIgnoreCase(String nome);

    Musica findByNome(String nome);
}

